using App;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Shouldly;

namespace Test
{
    [TestClass]
    public class WordSearchTests
    {
        private readonly WordSearch classUnderTest;
        private readonly char[][] testBoard;

        public WordSearchTests()
        {
            this.classUnderTest = new WordSearch();

            this.testBoard = new char[][]
            {
                new char[] { 'A', 'B', 'C', 'E' },
                new char[] { 'S', 'F', 'C', 'S' },
                new char[] { 'A', 'D', 'E', 'E' }
            };
        }

        [TestMethod]
        public void Test1()
        {
            this.classUnderTest.Exist(this.testBoard, "A").ShouldBeTrue();
        }

        [TestMethod]
        public void Test2()
        {
            this.classUnderTest.Exist(this.testBoard, "Z").ShouldBeFalse();
        }

        [TestMethod]
        public void Test3()
        {
            this.classUnderTest.Exist(this.testBoard, "ABC").ShouldBeTrue();
        }

        [TestMethod]
        public void Test4()
        {
            this.classUnderTest.Exist(this.testBoard, "ABCCED").ShouldBeTrue();
        }

        [TestMethod]
        public void Test5()
        {
            this.classUnderTest.Exist(this.testBoard, "SEE").ShouldBeTrue();
        }

        [TestMethod]
        public void Test6()
        {
            this.classUnderTest.Exist(this.testBoard, "ABCB").ShouldBeFalse();
        }
    }
}
